<%@include file="header.jsp" %>

	<spring:url value="/search" var="backUrl" />
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Comparison</h3>
		</div>
		<div class="panel-body">
			<div class="list-group">
				<c:forEach var="ratedFilm" items="${ratedFilms}" varStatus="loop">
					<spring:url value="/rate/${ratedFilm.film.imdbId}" var="actionUrl" />
					
					<div class="panel panel-default list-group-item">
					    <c:choose>
						    <c:when test="${loop.index == 0}">
				    			<h3 class="list-group-item-heading">${ratedFilm.film.title} (${ratedFilm.film.year}) <span class="label label-default">Winner</span></h3>
						    </c:when>
						    <c:otherwise>
				    			<h3 class="list-group-item-heading">${ratedFilm.film.title} (${ratedFilm.film.year})</h3>
						    </c:otherwise>
						</c:choose>
					    <c:choose>
						    <c:when test="${ratedFilm.film.posterUrl == 'N/A'}">
					   			<img src="resources/default.jpg" class="img-thumbnail" alt="Poster" width="304" height="236">
						    </c:when>
						    <c:otherwise>
					    		<img src="${ratedFilm.film.posterUrl}" class="img-thumbnail" alt="Poster" width="304" height="236">
						    </c:otherwise>
						</c:choose>
						<div class="list-group-item-text">
						    <div>${ratedFilm.film.plot}</div>
						    <div><b>Director:</b> ${ratedFilm.film.director}</div>
						    <div><b>IMDB rating:</b> ${ratedFilm.film.imdbRating}</div>
						    <c:choose>
							    <c:when test="${empty ratedFilm.rating}">
							        <div>
							        <b>Your rating:</b> N/A
							        <a href="${actionUrl}" class="btn btn-link" role="button">Rate now!</a>
							       	</div>
							    </c:when>
							    <c:otherwise>
							        <div><b>Your rating:</b> ${ratedFilm.rating}</div>
							    </c:otherwise>
							</c:choose>
						</div>
					</div>
				</c:forEach>
			</div>
			<a href="${backUrl}" class="btn btn-primary" role="button">Back</a>
		</div>
	
	</div>
	
<%@include file="footer.jsp" %>