<%@include file="header.jsp" %>

	<spring:url value="/rate" var="actionUrl" />
	
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Rate film</h3>
	  </div>
	  <div class="panel-body">		
		<div class="panel panel-default">
			<h3 class="list-group-item-heading">${film.title} (${film.year})</h3>
		    <c:choose>
			    <c:when test="${film.posterUrl == 'N/A'}">
		   			<img src="resources/default.jpg" class="img-thumbnail" alt="Poster" width="304" height="236">
			    </c:when>
			    <c:otherwise>
		    		<img src="${film.posterUrl}" class="img-thumbnail" alt="Poster" width="304" height="236">
			    </c:otherwise>
			</c:choose>
			<div class="list-group-item-text">
			    <div>${film.plot}</div>
			    <div><b>Director:</b> ${film.director}</div>
			    <div><b>IMDB rating:</b> ${film.imdbRating}</div>
			</div>
		</div>
	
		<form:form action="${actionUrl}" method="POST" modelAttribute="rateForm">
			<div class="form-group">
				<label for=rating>Rating:</label>
				<form:input id="rating" path="rating" type="text" />
				<form:hidden path="imdbId" value="${film.imdbId}"/>
			</div>
			
			<button type="submit" class="btn btn-primary">Submit</button>
		</form:form>
	  </div>
	</div>

<%@include file="footer.jsp" %>