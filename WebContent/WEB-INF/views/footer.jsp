		<c:if test="${not empty successMessage}">
			<div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
			  <strong>Success!</strong> ${successMessage}
			</div>
		</c:if>
		
			
		<c:if test="${not empty errorMessage}">
			<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
			  <strong>Error!</strong> ${errorMessage}
			</div>
		</c:if>
	
	</div>
	
</body>
</html>