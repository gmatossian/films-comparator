<%@include file="header.jsp" %>

	<div class="container">
	  <div class="jumbotron">
	    <h1>An error has occurred</h1> 
	    <p>Please contact the system administrator.</p> 
	  </div>
	</div>

<%@include file="footer.jsp" %>