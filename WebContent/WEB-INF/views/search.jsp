<%@include file="header.jsp" %>

	<spring:url value="/search" var="actionUrl" />
	
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Search for titles</h3>
	  </div>
	  <div class="panel-body">
		<form:form action="${actionUrl}" method="POST" modelAttribute="searchForm">
			<div class="form-group">
				<label for=firstTitle>Title 1:</label>
				<form:input id="firstTitle" path="title1" type="text" />
			</div>
			<div class="form-group">
				<label for=secondTitle>Title 2:</label>
				<form:input id="secondTitle" path="title2" type="text" />
			</div>
			
			<button type="submit" class="btn btn-primary">Search</button>
		</form:form>
	  </div>
	</div>

<%@include file="footer.jsp" %>