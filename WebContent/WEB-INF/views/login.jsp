<%@include file="header.jsp" %>

	<spring:url value="/login" var="actionUrl" />
	
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Login</h3>
	  </div>
	  <div class="panel-body">
		<form:form action="${actionUrl}" method="POST" modelAttribute="user">
			<div class="form-group">
				<label for="id">User ID:</label>
				<form:input id="id" path="username" type="text" />
			</div>
			
			<button type="submit" class="btn btn-primary">Submit</button>
		</form:form>
	  </div>
	</div>

<%@include file="footer.jsp" %>