<%@include file="header.jsp" %>

	<spring:url value="/compare" var="actionUrl" />
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Search results</h3>
		</div>
		<div class="panel-body">
			<div class="alert alert-info fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
				Please select movies to compare and then press the <strong>Compare</strong> button.
			</div>
	
			<form:form action="${actionUrl}" method="POST" modelAttribute="compareForm">
				<div class="row">
					<div class="col-sm-6">
							<div class="list-group">
								<c:forEach var="ratedFilm" items="${list1}">
									<spring:url value="/rate/${ratedFilm.film.imdbId}" var="actionUrl" />
									
									<div class="panel panel-default list-group-item">
								    	<h3 class="list-group-item-heading">${ratedFilm.film.title} (${ratedFilm.film.year})</h3>
									    <c:choose>
										    <c:when test="${ratedFilm.film.posterUrl == 'N/A'}">
									   			<img src="resources/default.jpg" class="img-thumbnail" alt="Poster" width="304" height="236">
										    </c:when>
										    <c:otherwise>
									    		<img src="${ratedFilm.film.posterUrl}" class="img-thumbnail" alt="Poster" width="304" height="236">
										    </c:otherwise>
										</c:choose>
										<div class="list-group-item-text">
										    <div>${ratedFilm.film.plot}</div>
										    <div><b>Director:</b> ${ratedFilm.film.director}</div>
										    <div><b>IMDB rating:</b> ${ratedFilm.film.imdbRating}</div>
										    <c:choose>
											    <c:when test="${empty ratedFilm.rating}">
											        <div>
											        <b>Your rating:</b> N/A
											        <a href="${actionUrl}" class="btn btn-link" role="button">Rate now!</a>
											       	</div>
											    </c:when>
											    <c:otherwise>
											        <div><b>Your rating:</b> ${ratedFilm.rating}</div>
											    </c:otherwise>
											</c:choose>
									    	<div><form:checkbox path="imdbIds" value="${ratedFilm.film.imdbId}"/> <b>Include in comparison</b></div>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="list-group">
								<c:forEach var="ratedFilm" items="${list2}">
									<spring:url value="/rate/${ratedFilm.film.imdbId}" var="actionUrl" />
									
									<div class="panel panel-default list-group-item">
								    	<h3 class="list-group-item-heading">${ratedFilm.film.title} (${ratedFilm.film.year})</h3>
									    <c:choose>
										    <c:when test="${ratedFilm.film.posterUrl == 'N/A'}">
									   			<img src="resources/default.jpg" class="img-thumbnail" alt="Poster" width="304" height="236">
										    </c:when>
										    <c:otherwise>
									    		<img src="${ratedFilm.film.posterUrl}" class="img-thumbnail" alt="Poster" width="304" height="236">
										    </c:otherwise>
										</c:choose>
										<div class="list-group-item-text">
										    <div>${ratedFilm.film.plot}</div>
										    <div><b>Director:</b> ${ratedFilm.film.director}</div>
										    <div><b>IMDB rating:</b> ${ratedFilm.film.imdbRating}</div>
										    <c:choose>
											    <c:when test="${empty ratedFilm.rating}">
											        <div>
											        <b>Your rating:</b> N/A
											        <a href="${actionUrl}" class="btn btn-link" role="button">Rate now!</a>
											       	</div>
											    </c:when>
											    <c:otherwise>
											        <div><b>Your rating:</b> ${ratedFilm.rating}</div>
											    </c:otherwise>
											</c:choose>
									    	<div><form:checkbox path="imdbIds" value="${ratedFilm.film.imdbId}"/> <b>Include in comparison</b></div>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
					</div>
				<button type="submit" class="btn btn-primary">Compare</button>
			</form:form>
		</div>
	</div>

<%@include file="footer.jsp" %>