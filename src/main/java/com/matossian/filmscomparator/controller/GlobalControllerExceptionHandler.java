package com.matossian.filmscomparator.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalControllerExceptionHandler {
	
	private static final Logger logger = LogManager.getLogger(GlobalControllerExceptionHandler.class);

	@ExceptionHandler(Exception.class)
    public String handleConflict(Exception e) {
		logger.error("Error", e);
        return "error";
    }
}
