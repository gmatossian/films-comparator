package com.matossian.filmscomparator.controller;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.matossian.filmscomparator.dao.UserNotFoundException;
import com.matossian.filmscomparator.form.LoginForm;
import com.matossian.filmscomparator.model.User;
import com.matossian.filmscomparator.service.FilmComparisonService;

@Controller
public class LoginController {
	
	private static final Logger logger = LogManager.getLogger(LoginController.class);
	
	@Autowired
	private FilmComparisonService filmComparisonService;
	
	public void setFilmComparisonService(FilmComparisonService filmComparisonService) {
		this.filmComparisonService = filmComparisonService;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showForm(Model model) {
		model.addAttribute("user", new LoginForm());
		
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String processForm(@ModelAttribute User user, HttpSession session) {
		User existingUser = null;
		
		try {
			existingUser = filmComparisonService.findUser(user.getUsername());
		} catch (UserNotFoundException e) {
			logger.info("User with username " + user.getUsername() + " is not registered");
		}
		
		if(existingUser == null) {
			session.setAttribute("user", user);
			filmComparisonService.registerUser(user);
		} else {
			session.setAttribute("user", existingUser);
		}
		
		return "redirect:/search";
	}

}
