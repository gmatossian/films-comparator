package com.matossian.filmscomparator.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.matossian.filmscomparator.model.User;

@Controller
public class HomeController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String redirect(HttpSession session) {
		User user = (User) session.getAttribute("user");
		
		if(user == null) {
			return "redirect:/login";
		} else {
			return "redirect:/search";
		}
	}

}
