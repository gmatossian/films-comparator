package com.matossian.filmscomparator.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.matossian.filmscomparator.form.RateForm;
import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.User;
import com.matossian.filmscomparator.service.FilmComparisonService;
import com.matossian.filmscomparator.service.FilmServiceException;

@Controller
public class RateController {
	
	@Autowired
	private FilmComparisonService filmComparisonService;
	
	public void setFilmComparisonService(FilmComparisonService filmComparisonService) {
		this.filmComparisonService = filmComparisonService;
	}

	@RequestMapping(value="/rate/{imdbId}", method=RequestMethod.GET)
	public String rateFilm(Model model, @PathVariable String imdbId) throws FilmServiceException {
		RateForm rateForm = new RateForm();
		Film film = filmComparisonService.findByImdbId(imdbId);
		
		model.addAttribute("rateForm", rateForm);
		model.addAttribute("film", film);
		
		return "rate";
	}
	
	@RequestMapping(value="/rate", method=RequestMethod.POST)
	public String processForm(@ModelAttribute RateForm form, HttpSession session, RedirectAttributes redirectAttributes) throws FilmServiceException {
		Film film = filmComparisonService.findByImdbId(form.getImdbId());
		filmComparisonService.rate((User) session.getAttribute("user"), film, form.getRating());
		
		redirectAttributes.addFlashAttribute("successMessage", "Rating saved correctly");
		
		return "redirect:/search";
	}
}
