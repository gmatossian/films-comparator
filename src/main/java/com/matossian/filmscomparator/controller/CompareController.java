package com.matossian.filmscomparator.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.matossian.filmscomparator.form.CompareForm;
import com.matossian.filmscomparator.model.RatedFilm;
import com.matossian.filmscomparator.model.User;
import com.matossian.filmscomparator.service.FilmComparisonService;
import com.matossian.filmscomparator.service.FilmServiceException;

@Controller
public class CompareController {
	
	@Autowired
	private FilmComparisonService filmComparisonService;

	public void setFilmComparisonService(FilmComparisonService filmComparisonService) {
		this.filmComparisonService = filmComparisonService;
	}

	@RequestMapping(value="/compare", method=RequestMethod.POST)
	public String processForm(@ModelAttribute CompareForm form, Model model, HttpSession session) throws FilmServiceException {
		User user = (User) session.getAttribute("user");
		List<RatedFilm> ratedFilms = filmComparisonService.compare(user, form.getImdbIds());
		
		model.addAttribute("ratedFilms", ratedFilms);
		
		return "comparison";
	}
}
