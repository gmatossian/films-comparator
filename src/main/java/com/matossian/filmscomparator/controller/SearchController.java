package com.matossian.filmscomparator.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.matossian.filmscomparator.form.CompareForm;
import com.matossian.filmscomparator.form.SearchForm;
import com.matossian.filmscomparator.model.User;
import com.matossian.filmscomparator.service.FilmComparisonService;

@Controller
public class SearchController {
	
	@Autowired
	private FilmComparisonService filmComparisonService;

	public void setFilmComparisonService(FilmComparisonService filmComparisonService) {
		this.filmComparisonService = filmComparisonService;
	}

	@RequestMapping(value="/search", method=RequestMethod.GET)
	public String showForm(Model model) {
		model.addAttribute("searchForm", new SearchForm());
		
		return "search";
	}
	
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public String processForm(@ModelAttribute SearchForm searchForm, Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		
		model.addAttribute("list1", filmComparisonService.findByTitle(user, searchForm.getTitle1()));
		model.addAttribute("list2", filmComparisonService.findByTitle(user, searchForm.getTitle2()));
		model.addAttribute("compareForm", new CompareForm());
		
		return "results";
	}
}
