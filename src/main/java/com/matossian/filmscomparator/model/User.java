package com.matossian.filmscomparator.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Class modeling a user of the application
 * 
 * @see Rating
 * 
 * @author gabo
 *
 */
public class User {

	private String username;
	private Map<String, Double> ratings = new HashMap<>();

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public Map<String, Double> getRatings() {
		return ratings;
	}

	public void setRatings(Map<String, Double> ratings) {
		this.ratings = ratings;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	public String toString() {
		return username;
	}
}
