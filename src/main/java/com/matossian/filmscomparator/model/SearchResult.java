package com.matossian.filmscomparator.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class modeling a films search result
 * @see Film
 * 
 * @author gabo
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResult {

	@JsonProperty("Search")
	private List<Film> films;
	private Integer totalResults;

	public List<Film> getFilms() {
		return films;
	}

	public void setFilms(List<Film> films) {
		this.films = films;
	}

	public Integer getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(Integer totalResults) {
		this.totalResults = totalResults;
	}
}
