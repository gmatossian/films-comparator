package com.matossian.filmscomparator.model;

/**
 * Class putting together both a {@link Film} and a rating provided by a {@link User}
 * 
 * @author gabo
 *
 */
public class RatedFilm {

	private Film film;
	private Double rating;

	public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}
}
