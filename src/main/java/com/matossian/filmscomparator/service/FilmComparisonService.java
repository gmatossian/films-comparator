package com.matossian.filmscomparator.service;

import java.util.List;

import com.matossian.filmscomparator.dao.UserNotFoundException;
import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.RatedFilm;
import com.matossian.filmscomparator.model.User;

/**
 * Service defining operations for searching for films, to be used by controllers
 * 
 * @author gabo
 *
 */
public interface FilmComparisonService {

	/**
	 * Method for searching for films by title
	 * @param title
	 * @return a list of {@link Film} objects sorted by rating (considering user rating over IMDB rating when applicable) 
	 */
	List<RatedFilm> findByTitle(User user, String title);
	
	/**
	 * Method for saving a user-defined rating for a particular film
	 * @param user
	 * @param film
	 * @param rating
	 */
	void rate(User user, Film film, Double rating);
	
	/**
	 * Method for registering a user
	 * @param user
	 */
	void registerUser(User user);
	
	/**
	 * Method for finding a user by username
	 * @param username
	 * @return
	 * @throws UserNotFoundException
	 */
	User findUser(String username) throws UserNotFoundException;

	/**
	 * Method for searching for a particular film by imdbId
	 * @param imdbId
	 * @return a {@link Film} object with imdbId matching the provided one (or null if not found) 
	 */
	Film findByImdbId(String imdbId) throws FilmServiceException;

	/**
	 * Method for comparing films
	 * @param user
	 * @param imdbIds
	 * @return a list of {@link RatedFilm} objects sorted by rating (considering user rating over IMDB rating when applicable) 
	 */
	List<RatedFilm> compare(User user, String[] imdbIds) throws FilmServiceException;
}
