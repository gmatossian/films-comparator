package com.matossian.filmscomparator.service;

public class FilmServiceException extends Exception {

	public FilmServiceException(String message) {
		super(message);
	}

	public FilmServiceException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
