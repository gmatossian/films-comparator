package com.matossian.filmscomparator.service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matossian.filmscomparator.controller.RateController;
import com.matossian.filmscomparator.dao.UserDao;
import com.matossian.filmscomparator.dao.UserNotFoundException;
import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.RatedFilm;
import com.matossian.filmscomparator.model.SearchResult;
import com.matossian.filmscomparator.model.User;

@Service
public class FilmComparisonServiceImpl implements FilmComparisonService {
	
	private static final Logger logger = LogManager.getLogger(FilmComparisonServiceImpl.class);
	
	@Autowired
	private UserDao userDao;
	private RatedFilmComparator comparator = new RatedFilmComparator();
	
	@Autowired
	private FilmService filmService;

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public FilmService getFilmService() {
		return filmService;
	}

	public void setFilmService(FilmService filmService) {
		this.filmService = filmService;
	}

	@Override
	public List<RatedFilm> findByTitle(User user, String title) {
		List<RatedFilm> ratedFilms = new LinkedList<>();
		try {
			SearchResult searchResult = filmService.findByTitle(title);
			for(Film film : searchResult.getFilms()) {
				ratedFilms.add(buildRatedFilm(user, film));
			}
			Collections.sort(ratedFilms, comparator);
			return ratedFilms;
		} catch (FilmServiceException e) {
			logger.error("Error finding films by title", e);
		}
		return ratedFilms;
	}

	@Override
	public void rate(User user, Film film, Double rating) {
		user.getRatings().put(film.getImdbId(), rating);
		
		userDao.update(user);
	}

	@Override
	public void registerUser(User user) {
		userDao.save(user);
	}

	@Override
	public User findUser(String username) throws UserNotFoundException {
		return userDao.getByUsername(username);
	}

	@Override
	public Film findByImdbId(String imdbId) throws FilmServiceException {
		return filmService.findByImdbId(imdbId);
	}

	@Override
	public List<RatedFilm> compare(User user, String[] imdbIds) throws FilmServiceException {
		List<RatedFilm> ratedFilms = new LinkedList<>();
		
		for(String imdbId : imdbIds) {
			Film film = filmService.findByImdbId(imdbId);
			ratedFilms.add(buildRatedFilm(user, film));
		}
		Collections.sort(ratedFilms, comparator);
		
		return ratedFilms;
	}
	
	private RatedFilm buildRatedFilm(User user, Film film) {
		RatedFilm ratedFilm = new RatedFilm();
		if(user.getRatings().containsKey(film.getImdbId())) {
			ratedFilm.setRating(user.getRatings().get(film.getImdbId()));
		}
		ratedFilm.setFilm(film);
		return ratedFilm;
	}

}
