package com.matossian.filmscomparator.service;

import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.matossian.filmscomparator.model.RatedFilm;

/**
 * Implementation of {@link Comparator} for comparing {@link RatedFilm} objects
 * sorting by rating descending, and considering user-defined ratings when applicable
 * @author gabo
 *
 */
public class RatedFilmComparator implements Comparator<RatedFilm> {
	
	private static final Logger logger = LogManager.getLogger(RatedFilmComparator.class);

	@Override
	public int compare(RatedFilm o1, RatedFilm o2) {
		Double imdbRating1 = 0D;
		Double imdbRating2 = 0D;
		try {
			imdbRating1 = Double.valueOf(o1.getFilm().getImdbRating());
		} catch (NumberFormatException e) {
			logger.debug("imdbRating " + o1.getFilm().getImdbRating() + " is not a valid double");
		}
		try {
			imdbRating2 = Double.valueOf(o2.getFilm().getImdbRating());
		} catch (NumberFormatException e) {
			logger.debug("imdbRating " + o2.getFilm().getImdbRating() + " is not a valid double");
		}
		Double rating1 = (o1.getRating() != null ? o1.getRating() : imdbRating1);
		Double rating2 = (o2.getRating() != null ? o2.getRating() : imdbRating2);
		return rating2.compareTo(rating1);
	}

}
