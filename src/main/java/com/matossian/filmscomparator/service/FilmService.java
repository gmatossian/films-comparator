package com.matossian.filmscomparator.service;

import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.SearchResult;

/**
 * Interface defining methods for films retrieval
 * 
 * @author gabo
 *
 */
public interface FilmService {

	/**
	 * Searches for films based on title
	 * @param title
	 * @return a {@link SearchResult} object
	 */
	SearchResult findByTitle(String title) throws FilmServiceException;

	/**
	 * Searches for a particular film based on its imdb id
	 * @param imdbId
	 * @return a {@link Film} object
	 */
	Film findByImdbId(String imdbId) throws FilmServiceException;
}
