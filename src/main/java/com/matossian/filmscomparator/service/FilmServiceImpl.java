package com.matossian.filmscomparator.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.SearchResult;

@Service
public class FilmServiceImpl implements FilmService {
	
	@Autowired
	private RestTemplate restTemplate;

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public SearchResult findByTitle(String title) throws FilmServiceException {
		try {
			ResponseEntity<SearchResult> responseEntity = restTemplate.getForEntity("http://www.omdbapi.com/?s={title}", SearchResult.class, title);
			SearchResult searchResult = responseEntity.getBody();
			
			List<Film> enrichedFilms = new LinkedList<>();
			if(!searchResult.getFilms().isEmpty()) {
				for(Film film : searchResult.getFilms()) {
					Film enrichedFilm = findByImdbId(film.getImdbId());
					enrichedFilms.add(enrichedFilm);
				}
			}
			searchResult.setFilms(enrichedFilms);
			
			return searchResult;
		} catch (HttpStatusCodeException e) {
			throw new FilmServiceException("Error searching for films by title", e);
		}
	}

	@Override
	public Film findByImdbId(String imdbId) throws FilmServiceException {
		Film film = null;
		try {
			film = restTemplate.getForObject("http://www.omdbapi.com/?i={id}", Film.class, imdbId);
		} catch (HttpStatusCodeException e) {
			throw new FilmServiceException("Error searching for film by imdbId", e);
		}
		if(film.getImdbId() == null) {
			throw new FilmServiceException("No Film found for imdbId " + imdbId);
		}
		return film;
	}

}
