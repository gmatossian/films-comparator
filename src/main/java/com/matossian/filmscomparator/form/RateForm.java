package com.matossian.filmscomparator.form;

/**
 * Form-backing class
 * @author gabo
 *
 */
public class RateForm {

	private String imdbId;
	private Double rating;

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}
}
