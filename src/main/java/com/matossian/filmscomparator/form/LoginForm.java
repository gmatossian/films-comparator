package com.matossian.filmscomparator.form;

/**
 * Form-backing class
 * @author gabo
 *
 */
public class LoginForm {

	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
