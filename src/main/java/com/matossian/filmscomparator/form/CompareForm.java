package com.matossian.filmscomparator.form;

public class CompareForm {

	private String [] imdbIds;

	public String[] getImdbIds() {
		return imdbIds;
	}

	public void setImdbIds(String[] imdbIds) {
		this.imdbIds = imdbIds;
	}
}
