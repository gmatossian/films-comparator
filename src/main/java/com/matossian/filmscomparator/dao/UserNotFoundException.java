package com.matossian.filmscomparator.dao;

import com.matossian.filmscomparator.model.User;

/**
 * Exception to be thrown when a {@link User} is not found
 * 
 * @author gabo
 *
 */
public class UserNotFoundException extends Exception {

	public UserNotFoundException(String message) {
		super(message);
	}

	public UserNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
