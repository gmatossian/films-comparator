package com.matossian.filmscomparator.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.matossian.filmscomparator.model.User;

@Component
public class UserDaoImpl implements UserDao {
	
	private Map<String, User> users = new HashMap<>();

	public Map<String, User> getUsers() {
		return users;
	}

	public void setUsers(Map<String, User> users) {
		this.users = users;
	}

	@Override
	public User getByUsername(String username) throws UserNotFoundException {
		User user = users.get(username);
		if(user == null) {
			throw new UserNotFoundException("User with username " + username + " not found");
		}
		return user;
	}

	@Override
	public void update(User user) {
		if(users.containsKey(user.getUsername())) {
			users.put(user.getUsername(), user);
		} else {
			throw new IllegalArgumentException("The user is not registered");
		}
	}

	@Override
	public void save(User user) {
		if(users.containsKey(user.getUsername())) {
			throw new IllegalArgumentException("The user is already registered");
		} else {
			users.put(user.getUsername(), user);
		}
	}

}
