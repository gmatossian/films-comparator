package com.matossian.filmscomparator.dao;

import com.matossian.filmscomparator.model.User;

/**
 * Data access object defining methods for manipulating {@link User} objects
 * 
 * @author gabo
 *
 */
public interface UserDao {

	/**
	 * Finds a {@link User} by username
	 * @param username the username
	 * @return A {@link User} with the provided username or null
	 */
	User getByUsername(String username) throws UserNotFoundException;
	
	/**
	 * Updates a {@link User}
	 * @param user
	 */
	void update(User user);
	
	/**
	 * Saves a {@link User}
	 * @param user
	 */
	void save(User user);
}
