package com.matossian.filmscomparator.dao;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.User;

public class UserDaoImplTest {

	@Test
	public void testGetByUsernameWithRegisteredUsername() throws UserNotFoundException{
		UserDaoImpl userDao = new UserDaoImpl();
		
		User user = new User();
		user.setUsername("gmatossian");
		user.setRatings(new HashMap<String, Double>());
		userDao.getUsers().put("gmatossian", user);
		
		User result = userDao.getByUsername("gmatossian");
		
		assertEquals(user, result);
	}

	@Test(expected=UserNotFoundException.class)
	public void testGetByUsernameWithUnregisteredUsername() throws UserNotFoundException{
		UserDaoImpl userDao = new UserDaoImpl();
		
		User result = userDao.getByUsername("msossich");
	}
	
	@Test
	public void testUpdate() {
		UserDaoImpl userDao = new UserDaoImpl();
		
		User user = new User();
		user.setUsername("gmatossian");
		user.getRatings().put("24857", 8.2);
		userDao.getUsers().put("gmatossian", user);
		
		User updatedUser = new User();
		updatedUser.setUsername("gmatossian");
		updatedUser.getRatings().put("24857", 8.3);
		updatedUser.getRatings().put("47845", 7.8);
		
		userDao.update(updatedUser);
		
		assertEquals(updatedUser, userDao.getUsers().get("gmatossian"));
		assertEquals(updatedUser.getRatings(), userDao.getUsers().get("gmatossian").getRatings());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testUpdateUnregisteredUser() {
		UserDaoImpl userDao = new UserDaoImpl();
		
		User user = new User();
		user.setUsername("gmatossian");
		user.getRatings().put("24857", 8.2);
		userDao.getUsers().put("gmatossian", user);
		
		User updatedUser = new User();
		updatedUser.setUsername("msossich");
		updatedUser.getRatings().put("24857", 8.3);
		updatedUser.getRatings().put("47845", 7.8);
		
		userDao.update(updatedUser);
	}
	
	@Test
	public void testSave() {
		UserDaoImpl userDao = new UserDaoImpl();
		
		User user = new User();
		user.setUsername("gmatossian");
		user.getRatings().put("24857", 8.2);
		
		userDao.save(user);
		
		assertEquals(user, userDao.getUsers().get("gmatossian"));
		assertEquals(user.getRatings(), userDao.getUsers().get("gmatossian").getRatings());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSaveRegisteredUser() {
		UserDaoImpl userDao = new UserDaoImpl();
		
		User user = new User();
		user.setUsername("gmatossian");
		user.getRatings().put("24857", 8.2);
		userDao.getUsers().put("gmatossian", user);
		
		userDao.save(user);
	}

}
