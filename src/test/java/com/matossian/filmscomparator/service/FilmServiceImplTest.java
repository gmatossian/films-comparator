package com.matossian.filmscomparator.service;

import static org.junit.Assert.*;
import static org.springframework.test.web.client.ExpectedCount.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.SearchResult;

public class FilmServiceImplTest {

	@Test
	public void testFindByTitle() throws FilmServiceException {
		FilmServiceImpl filmService = new FilmServiceImpl();
		
		RestTemplate restTemplate = new RestTemplate();
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(once(), requestTo("http://www.omdbapi.com/?s=Avatar")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(new ClassPathResource("search_by_title_result_avatar.json"), MediaType.APPLICATION_JSON));
		server.expect(once(), requestTo("http://www.omdbapi.com/?i=tt0499549")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(new ClassPathResource("search_by_id_result_avatar.json"), MediaType.APPLICATION_JSON));
		
		filmService.setRestTemplate(restTemplate);
		
		SearchResult searchResult = filmService.findByTitle("Avatar");

		assertEquals(Integer.valueOf(1), searchResult.getTotalResults());
		Film film = searchResult.getFilms().get(0);
		assertEquals("Avatar", film.getTitle());
		assertEquals("2009", film.getYear());
		assertEquals("tt0499549", film.getImdbId());
		assertEquals("movie", film.getType());
		assertEquals("https://images-na.ssl-images-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_SX300.jpg", film.getPosterUrl());

		server.verify();
	}

	@Test(expected=FilmServiceException.class)
	public void testFindByTitle_404() throws FilmServiceException {
		FilmServiceImpl filmService = new FilmServiceImpl();
		
		RestTemplate restTemplate = new RestTemplate();
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(once(), requestTo("http://www.omdbapi.com/?s=Avatar")).andExpect(method(HttpMethod.GET))
				.andRespond(withStatus(HttpStatus.NOT_FOUND));
		
		filmService.setRestTemplate(restTemplate);
		
		SearchResult searchResult = filmService.findByTitle("Avatar");

		server.verify();
	}

	@Test(expected=FilmServiceException.class)
	public void testFindByTitle_500() throws FilmServiceException {
		FilmServiceImpl filmService = new FilmServiceImpl();
		
		RestTemplate restTemplate = new RestTemplate();
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(once(), requestTo("http://www.omdbapi.com/?s=Avatar")).andExpect(method(HttpMethod.GET))
				.andRespond(withServerError());
		
		filmService.setRestTemplate(restTemplate);
		
		SearchResult searchResult = filmService.findByTitle("Avatar");

		server.verify();
	}

	@Test
	public void testFindByTitleEnrichment() throws FilmServiceException {
		FilmServiceImpl filmService = new FilmServiceImpl();
		RestTemplate restTemplate = new RestTemplate();
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		filmService.setRestTemplate(restTemplate);
		
		server.expect(once(), requestTo("http://www.omdbapi.com/?s=Terminator")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(new ClassPathResource("search_by_title_result_terminator.json"), MediaType.APPLICATION_JSON));
		server.expect(once(), requestTo("http://www.omdbapi.com/?i=tt0103064")).andExpect(method(HttpMethod.GET))
			.andRespond(withSuccess(new ClassPathResource("search_by_id_result_terminator2.json"), MediaType.APPLICATION_JSON));
		server.expect(once(), requestTo("http://www.omdbapi.com/?i=tt0088247")).andExpect(method(HttpMethod.GET))
			.andRespond(withSuccess(new ClassPathResource("search_by_id_result_terminator.json"), MediaType.APPLICATION_JSON));
		
		SearchResult searchResult = filmService.findByTitle("Terminator");

		server.verify();

		assertEquals(Integer.valueOf(76), searchResult.getTotalResults());
		
		assertEquals(Integer.valueOf(2), Integer.valueOf(searchResult.getFilms().size()));
		
		Film terminator2 = searchResult.getFilms().get(0);
		assertEquals("James Cameron", terminator2.getDirector());
		assertEquals("137 min", terminator2.getDuration());
		assertEquals("A cyborg, identical to the one who failed to kill Sarah Connor, must now protect her young son, John Connor, from a more advanced cyborg, made out of liquid metal.", terminator2.getPlot());
		assertEquals("English, Spanish", terminator2.getLanguage());
		assertEquals("75", terminator2.getMetascore());

		Film terminator = searchResult.getFilms().get(1);
		assertEquals("James Cameron", terminator.getDirector());
		assertEquals("107 min", terminator.getDuration());
		assertEquals("A human-looking indestructible cyborg is sent from 2029 to 1984 to assassinate a waitress, whose unborn son will lead humanity in a war against the machines, while a soldier from that war is sent to protect her at all costs.", terminator.getPlot());
		assertEquals("English, Spanish", terminator.getLanguage());
		assertEquals("83", terminator.getMetascore());
	}

	@Test
	public void testFindByImdbId() throws FilmServiceException {
		FilmServiceImpl filmService = new FilmServiceImpl();
		
		RestTemplate restTemplate = new RestTemplate();
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(once(), requestTo("http://www.omdbapi.com/?i=tt0499549")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(new ClassPathResource("search_by_id_result_avatar.json"), MediaType.APPLICATION_JSON));
		
		filmService.setRestTemplate(restTemplate);
		
		Film film = filmService.findByImdbId("tt0499549");

		assertEquals("Avatar", film.getTitle());
		assertEquals("2009", film.getYear());
		assertEquals("tt0499549", film.getImdbId());
		assertEquals("movie", film.getType());
		assertEquals("https://images-na.ssl-images-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_SX300.jpg", film.getPosterUrl());

		server.verify();
	}

	@Test(expected=FilmServiceException.class)
	public void testFindByImdbId_404() throws FilmServiceException {
		FilmServiceImpl filmService = new FilmServiceImpl();
		
		RestTemplate restTemplate = new RestTemplate();
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(once(), requestTo("http://www.omdbapi.com/?i=tt0499549")).andExpect(method(HttpMethod.GET))
				.andRespond(withStatus(HttpStatus.NOT_FOUND));
		
		filmService.setRestTemplate(restTemplate);
		
		Film film = filmService.findByImdbId("tt0499549");

		server.verify();
	}
	
	@Test(expected=FilmServiceException.class)
	public void testFindByImdbId_500() throws FilmServiceException {
		FilmServiceImpl filmService = new FilmServiceImpl();
		
		RestTemplate restTemplate = new RestTemplate();
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(once(), requestTo("http://www.omdbapi.com/?i=tt0499549")).andExpect(method(HttpMethod.GET))
				.andRespond(withServerError());
		
		filmService.setRestTemplate(restTemplate);
		
		Film film = filmService.findByImdbId("tt0499549");

		server.verify();
	}
	
	@Test(expected=FilmServiceException.class)
	public void testFindByImdbIdIncorrectImdbId() throws FilmServiceException {
		FilmServiceImpl filmService = new FilmServiceImpl();
		
		RestTemplate restTemplate = new RestTemplate();
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(once(), requestTo("http://www.omdbapi.com/?i=tt0499549")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(new ClassPathResource("error_response_incorrect_imdbid.json"), MediaType.APPLICATION_JSON));
		
		filmService.setRestTemplate(restTemplate);
		
		Film film = filmService.findByImdbId("tt0499549");

		server.verify();
	}

	@Test
	public void testFindByImdbIdNoImdRating() throws FilmServiceException {
		FilmServiceImpl filmService = new FilmServiceImpl();
		
		RestTemplate restTemplate = new RestTemplate();
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(once(), requestTo("http://www.omdbapi.com/?i=tt2447904")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(new ClassPathResource("search_by_id_result_pear.json"), MediaType.APPLICATION_JSON));
		
		filmService.setRestTemplate(restTemplate);
		
		Film film = filmService.findByImdbId("tt2447904");

		assertEquals("Pear", film.getTitle());
		assertEquals("2013", film.getYear());
		assertEquals("tt2447904", film.getImdbId());
		assertEquals("movie", film.getType());
		assertEquals("N/A", film.getPosterUrl());

		server.verify();
	}

}
