package com.matossian.filmscomparator.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.matossian.filmscomparator.dao.UserDao;
import com.matossian.filmscomparator.dao.UserDaoImpl;
import com.matossian.filmscomparator.dao.UserNotFoundException;
import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.RatedFilm;
import com.matossian.filmscomparator.model.SearchResult;
import com.matossian.filmscomparator.model.User;

public class FilmComparisonServiceTest {

	@Test
	public void testFindByTitle() throws UserNotFoundException, FilmServiceException {
		FilmComparisonServiceImpl filmComparisonService = new FilmComparisonServiceImpl();
		
		UserDao userDao = mock(UserDao.class);
		User user = new User();
		user.setUsername("gmatossian");
		when(userDao.getByUsername("gmatossian")).thenReturn(user);
		
		FilmService filmService = mock(FilmService.class);
		SearchResult searchResult = new SearchResult();
		searchResult.setTotalResults(2);
		List<Film> films = new LinkedList<>();
		Film batman = new Film();
			batman.setTitle("Batman");
			batman.setYear("2009");
			batman.setImdbId("3bjk321");
			batman.setImdbRating("7.3");
		films.add(batman);
		Film batmanReturns = new Film();
			batmanReturns.setTitle("Batman returns");
			batmanReturns.setYear("2013");
			batmanReturns.setImdbId("b235kjn");
			batmanReturns.setImdbRating("7.8");
		films.add(batmanReturns);
		searchResult.setFilms(films);
		when(filmService.findByTitle("Batman")).thenReturn(searchResult);
		
		filmComparisonService.setUserDao(userDao);
		filmComparisonService.setFilmService(filmService);
		
		List<RatedFilm> ratedFilms = filmComparisonService.findByTitle(user, "Batman");
		
		assertEquals(batmanReturns, ratedFilms.get(0).getFilm());
		assertNull(ratedFilms.get(0).getRating());
		assertEquals(batman, ratedFilms.get(1).getFilm());
		assertNull(ratedFilms.get(1).getRating());
	}

	@Test
	public void testFindByTitleWithRating() throws UserNotFoundException, FilmServiceException {
		FilmComparisonServiceImpl filmComparisonService = new FilmComparisonServiceImpl();
		
		UserDao userDao = mock(UserDao.class);
		User user = new User();
		user.setUsername("gmatossian");
		user.getRatings().put("3bjk321", 7.9);
		when(userDao.getByUsername("gmatossian")).thenReturn(user);
		
		FilmService filmService = mock(FilmService.class);
		SearchResult searchResult = new SearchResult();
		searchResult.setTotalResults(2);
		List<Film> films = new LinkedList<>();
		Film batman = new Film();
			batman.setTitle("Batman");
			batman.setYear("2009");
			batman.setImdbId("3bjk321");
			batman.setImdbRating("7.3");
		films.add(batman);
		Film batmanReturns = new Film();
			batmanReturns.setTitle("Batman returns");
			batmanReturns.setYear("2013");
			batmanReturns.setImdbId("b235kjn");
			batmanReturns.setImdbRating("7.8");
		films.add(batmanReturns);
		searchResult.setFilms(films);
		when(filmService.findByTitle("Batman")).thenReturn(searchResult);
		
		filmComparisonService.setUserDao(userDao);
		filmComparisonService.setFilmService(filmService);
		
		List<RatedFilm> ratedFilms = filmComparisonService.findByTitle(user, "Batman");
		
		assertEquals(batman, ratedFilms.get(0).getFilm());
		assertEquals(Double.valueOf(7.9), ratedFilms.get(0).getRating());
		assertEquals(batmanReturns, ratedFilms.get(1).getFilm());
		assertNull(ratedFilms.get(1).getRating());
	}

	@Test
	public void testFindByTitleThrowsException() throws UserNotFoundException, FilmServiceException {
		FilmComparisonServiceImpl filmComparisonService = new FilmComparisonServiceImpl();
		
		UserDao userDao = mock(UserDao.class);
		User user = new User();
		user.setUsername("gmatossian");
		when(userDao.getByUsername("gmatossian")).thenReturn(user);
		
		FilmService filmService = mock(FilmService.class);
		when(filmService.findByTitle("Batman")).thenThrow(new FilmServiceException("No films found for title Batman"));
		
		filmComparisonService.setUserDao(userDao);
		filmComparisonService.setFilmService(filmService);
		
		List<RatedFilm> ratedFilms = filmComparisonService.findByTitle(user, "Batman");
		
		assertTrue(ratedFilms.isEmpty());
	}
	
	@Test
	public void testRate() {
		FilmComparisonServiceImpl comparisonService = new FilmComparisonServiceImpl();
		
		UserDao userDao = mock(UserDao.class);
		FilmService filmService = mock(FilmService.class);
		
		comparisonService.setUserDao(userDao);
		comparisonService.setFilmService(filmService);
		
		User user = new User();
		user.setUsername("gmatossian");
		
		Film film = new Film();
		film.setImdbId("2389732");
		
		comparisonService.rate(user, film, 7.1);
		
		verify(userDao).update(user);
		
		assertEquals(Integer.valueOf(1), Integer.valueOf(user.getRatings().size()));
		assertTrue(user.getRatings().containsKey("2389732"));
		assertEquals(Double.valueOf(7.1), user.getRatings().get("2389732"));
	}
	
	@Test
	public void testRegisterUser() {
		FilmComparisonServiceImpl comparisonService = new FilmComparisonServiceImpl();
		UserDao userDao = mock(UserDao.class);
		comparisonService.setUserDao(userDao);
		
		User user = new User();
		user.setUsername("gmatossian");
		
		comparisonService.registerUser(user);
		
		verify(userDao).save(user);
	}
	
	@Test
	public void testFindUser() throws UserNotFoundException {
		FilmComparisonServiceImpl comparisonService = new FilmComparisonServiceImpl();
		UserDao userDao = mock(UserDao.class);
		comparisonService.setUserDao(userDao);
		
		comparisonService.findUser("gmatossian");
		
		verify(userDao).getByUsername("gmatossian");
	}
	
	@Test
	public void testFindByImdbId() throws FilmServiceException {
		FilmComparisonServiceImpl comparisonService = new FilmComparisonServiceImpl();
		FilmService filmService = mock(FilmService.class);
		comparisonService.setFilmService(filmService);
		
		comparisonService.findByImdbId("tt0499549");
		
		verify(filmService).findByImdbId("tt0499549");
	}
	
	@Test
	public void testCompareNoUserRatings() throws FilmServiceException {
		FilmComparisonServiceImpl comparisonService = new FilmComparisonServiceImpl();
		FilmService filmService = mock(FilmService.class);
		comparisonService.setFilmService(filmService);
		
		Film avatar = new Film();
		avatar.setTitle("Avatar");
		avatar.setImdbId("tt0499549");
		avatar.setImdbRating("7.9");
		Film terminator = new Film();
		terminator.setTitle("The Terminator");
		terminator.setImdbId("tt0088247");
		terminator.setImdbRating("8.1");
		
		when(filmService.findByImdbId("tt0499549")).thenReturn(avatar);
		when(filmService.findByImdbId("tt0088247")).thenReturn(terminator);
		
		User user = new User();
		String [] imdbIds = {"tt0499549", "tt0088247"};
		
		List<RatedFilm> films = comparisonService.compare(user, imdbIds);
		
		verify(filmService).findByImdbId("tt0499549");
		verify(filmService).findByImdbId("tt0088247");
		
		assertEquals(Integer.valueOf(2), Integer.valueOf(films.size()));
		
		assertEquals("The Terminator", films.get(0).getFilm().getTitle());
		assertEquals("tt0088247", films.get(0).getFilm().getImdbId());
		assertEquals("8.1", films.get(0).getFilm().getImdbRating());
		assertNull(films.get(0).getRating());
		
		assertEquals("Avatar", films.get(1).getFilm().getTitle());
		assertEquals("tt0499549", films.get(1).getFilm().getImdbId());
		assertEquals("7.9", films.get(1).getFilm().getImdbRating());
		assertNull(films.get(1).getRating());
	}
	
	@Test
	public void testCompareUserRatings() throws FilmServiceException {
		FilmComparisonServiceImpl comparisonService = new FilmComparisonServiceImpl();
		FilmService filmService = mock(FilmService.class);
		comparisonService.setFilmService(filmService);
		
		Film avatar = new Film();
		avatar.setTitle("Avatar");
		avatar.setImdbId("tt0499549");
		avatar.setImdbRating("7.9");
		Film terminator = new Film();
		terminator.setTitle("The Terminator");
		terminator.setImdbId("tt0088247");
		terminator.setImdbRating("8.1");
		
		when(filmService.findByImdbId("tt0499549")).thenReturn(avatar);
		when(filmService.findByImdbId("tt0088247")).thenReturn(terminator);
		
		User user = new User();
		user.getRatings().put("tt0499549", 8.3);
		String [] imdbIds = {"tt0499549", "tt0088247"};
		
		List<RatedFilm> films = comparisonService.compare(user, imdbIds);
		
		verify(filmService).findByImdbId("tt0499549");
		verify(filmService).findByImdbId("tt0088247");
		
		assertEquals(Integer.valueOf(2), Integer.valueOf(films.size()));
		
		assertEquals("Avatar", films.get(0).getFilm().getTitle());
		assertEquals("tt0499549", films.get(0).getFilm().getImdbId());
		assertEquals("7.9", films.get(0).getFilm().getImdbRating());
		assertEquals(Double.valueOf(8.3), films.get(0).getRating());
		
		assertEquals("The Terminator", films.get(1).getFilm().getTitle());
		assertEquals("tt0088247", films.get(1).getFilm().getImdbId());
		assertEquals("8.1", films.get(1).getFilm().getImdbRating());
		assertNull(films.get(1).getRating());
	}

}
