package com.matossian.filmscomparator.service;

import static org.junit.Assert.*;

import org.junit.Test;

import com.matossian.filmscomparator.controller.GlobalControllerExceptionHandler;

public class GlobalControllerExceptionHandlerTest {

	@Test
	public void test() {
		GlobalControllerExceptionHandler handler = new GlobalControllerExceptionHandler();
		
		String view = handler.handleConflict(new Exception());
		
		assertEquals("error", view);
	}

}
