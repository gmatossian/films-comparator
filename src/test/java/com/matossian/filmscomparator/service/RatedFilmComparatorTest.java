package com.matossian.filmscomparator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.RatedFilm;

public class RatedFilmComparatorTest {

	@Test
	public void testCompareWithNoUserRatings() {
		RatedFilmComparator comparator = new RatedFilmComparator();
		
		Film avatar = new Film();
		avatar.setImdbId("tt0499549");
		avatar.setImdbRating("7.9");
		RatedFilm ratedAvatar = new RatedFilm();
		ratedAvatar.setFilm(avatar);
		
		Film terminator = new Film();
		terminator.setImdbId("tt0088247");
		terminator.setImdbRating("8.1");
		RatedFilm ratedTerminator = new RatedFilm();
		ratedTerminator.setFilm(terminator);
		
		int result = comparator.compare(ratedAvatar, ratedTerminator);
		
		assertTrue(result > 0);
	}

	@Test
	public void testCompareEqualRatings() {
		RatedFilmComparator comparator = new RatedFilmComparator();
		
		Film avatar = new Film();
		avatar.setImdbId("tt0499549");
		avatar.setImdbRating("8.0");
		RatedFilm ratedAvatar = new RatedFilm();
		ratedAvatar.setFilm(avatar);
		
		Film terminator = new Film();
		terminator.setImdbId("tt0088247");
		terminator.setImdbRating("8.0");
		RatedFilm ratedTerminator = new RatedFilm();
		ratedTerminator.setFilm(terminator);
		
		int result = comparator.compare(ratedAvatar, ratedTerminator);
		
		assertEquals(Integer.valueOf(0), Integer.valueOf(result));
	}

	@Test
	public void testCompareWithUserRating() {
		RatedFilmComparator comparator = new RatedFilmComparator();
		
		Film avatar = new Film();
		avatar.setImdbId("tt0499549");
		avatar.setImdbRating("7.9");
		RatedFilm ratedAvatar = new RatedFilm();
		ratedAvatar.setFilm(avatar);
		ratedAvatar.setRating(8.3);
		
		Film terminator = new Film();
		terminator.setImdbId("tt0088247");
		terminator.setImdbRating("8.1");
		RatedFilm ratedTerminator = new RatedFilm();
		ratedTerminator.setFilm(terminator);
		
		int result = comparator.compare(ratedAvatar, ratedTerminator);
		
		assertTrue(result < 0);
	}

}
