package com.matossian.filmscomparator.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpSession;

import org.junit.Test;

import com.matossian.filmscomparator.dao.UserNotFoundException;
import com.matossian.filmscomparator.model.User;

public class HomeControllerTest {

	@Test
	public void testRedirectUserNotInSession() throws UserNotFoundException {
		HomeController controller = new HomeController();
		
		HttpSession session = mock(HttpSession.class);
		when(session.getAttribute("user")).thenReturn(null);
		
		String view = controller.redirect(session);
		
		verify(session).getAttribute("user");
		
		assertEquals("redirect:/login", view);
	}

	@Test
	public void testRedirectUserInSession() throws UserNotFoundException {
		HomeController controller = new HomeController();
		
		HttpSession session = mock(HttpSession.class);
		User user = new User();
		user.setUsername("gmatossian");
		when(session.getAttribute("user")).thenReturn(user);
		
		String view = controller.redirect(session);
		
		verify(session).getAttribute("user");
		
		assertEquals("redirect:/search", view);
	}

}
