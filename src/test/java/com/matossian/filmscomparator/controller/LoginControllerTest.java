package com.matossian.filmscomparator.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.springframework.ui.Model;

import com.matossian.filmscomparator.dao.UserNotFoundException;
import com.matossian.filmscomparator.form.LoginForm;
import com.matossian.filmscomparator.model.User;
import com.matossian.filmscomparator.service.FilmComparisonService;

public class LoginControllerTest {

	@Test
	public void test() {
		LoginController controller = new LoginController();
		Model model = mock(Model.class);
		
		String view = controller.showForm(model);
		
		verify(model).addAttribute(eq("user"), isA(LoginForm.class));
		
		assertEquals("login", view);
	}
	
	@Test
	public void testProcessFormExistingUser() throws UserNotFoundException {
		LoginController controller = new LoginController();
		FilmComparisonService service = mock(FilmComparisonService.class);
		controller.setFilmComparisonService(service);
		
		User user = new User();
		user.setUsername("gmatossian");
		
		when(service.findUser("gmatossian")).thenReturn(user);
		
		HttpSession session = mock(HttpSession.class);
		
		String view = controller.processForm(user, session);
		
		verify(session).setAttribute("user", user);
		
		assertEquals("redirect:/search", view);
	}
	
	@Test
	public void testProcessFormUnregisteredUser() throws UserNotFoundException {
		LoginController controller = new LoginController();
		FilmComparisonService service = mock(FilmComparisonService.class);
		controller.setFilmComparisonService(service);
		
		User user = new User();
		user.setUsername("gmatossian");
		
		when(service.findUser("gmatossian")).thenThrow(new UserNotFoundException ("User with username gmatossian not found"));
		
		HttpSession session = mock(HttpSession.class);
		
		String view = controller.processForm(user, session);
		
		verify(session).setAttribute("user", user);

		verify(service).registerUser(user);
		
		assertEquals("redirect:/search", view);
	}

}
