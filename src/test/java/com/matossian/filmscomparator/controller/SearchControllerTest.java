package com.matossian.filmscomparator.controller;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.springframework.ui.Model;

import com.matossian.filmscomparator.form.CompareForm;
import com.matossian.filmscomparator.form.SearchForm;
import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.RatedFilm;
import com.matossian.filmscomparator.model.User;
import com.matossian.filmscomparator.service.FilmComparisonService;

public class SearchControllerTest {

	@Test
	public void testShowForm() {
		SearchController controller = new SearchController();
		Model model = mock(Model.class);
		
		String view = controller.showForm(model);
		
		verify(model).addAttribute(eq("searchForm"), any(SearchForm.class));
		
		assertEquals("search", view);
	}

	@Test
	public void testProcessForm() {
		SearchController controller = new SearchController();
		FilmComparisonService service = mock(FilmComparisonService.class);
		controller.setFilmComparisonService(service);
		Model model = mock(Model.class);
		HttpSession session = mock(HttpSession.class);
		
		User user = new User();
		user.setUsername("gmatossian");
		
		List<RatedFilm> avatarFilms = new LinkedList<>();
		Film avatar = new Film();
			avatar.setTitle("Avatar");
			avatar.setImdbId("tt0499549");
		RatedFilm ratedAvatar = new RatedFilm();
		ratedAvatar.setFilm(avatar);
		avatarFilms.add(ratedAvatar);
		
		List<RatedFilm> terminatorFilms = new LinkedList<>();
		Film terminator = new Film();
			terminator.setTitle("The Terminator");
			terminator.setImdbId("tt0088247");
		RatedFilm ratedTerminator = new RatedFilm();
		ratedTerminator.setFilm(terminator);
		terminatorFilms.add(ratedTerminator);
		Film terminator2 = new Film();
			terminator2.setTitle("Terminator 2: Judgment Day");
			terminator2.setImdbId("tt0103064");
		RatedFilm ratedTerminator2 = new RatedFilm();
		ratedTerminator2.setFilm(terminator2);
		terminatorFilms.add(ratedTerminator2);
		
		when(service.findByTitle(user, "Avatar")).thenReturn(avatarFilms);
		when(service.findByTitle(user, "Terminator")).thenReturn(terminatorFilms);
		
		when(session.getAttribute("user")).thenReturn(user);

		SearchForm form = new SearchForm();
		form.setTitle1("Avatar");
		form.setTitle2("Terminator");
		
		String view = controller.processForm(form, model, session);
		
		verify(model).addAttribute("list1", avatarFilms);
		verify(model).addAttribute("list2", terminatorFilms);
		verify(model).addAttribute(eq("compareForm"), isA(CompareForm.class));
		
		assertEquals("results", view);
	}

}
