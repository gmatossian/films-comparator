package com.matossian.filmscomparator.controller;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.springframework.ui.Model;

import com.matossian.filmscomparator.form.CompareForm;
import com.matossian.filmscomparator.model.User;
import com.matossian.filmscomparator.service.FilmComparisonService;
import com.matossian.filmscomparator.service.FilmServiceException;

public class CompareControllerTest {

	@Test
	public void testProcessForm() throws FilmServiceException {
		CompareController controller = new CompareController();
		FilmComparisonService filmComparisonService = mock(FilmComparisonService.class);
		controller.setFilmComparisonService(filmComparisonService);
		
		Model model = mock(Model.class);
		HttpSession session = mock(HttpSession.class);
		User user = new User();
		when(session.getAttribute("user")).thenReturn(user);
		
		CompareForm form = new CompareForm();
		String [] imdbIds = {"tt0499549", "tt0088247"};
		form.setImdbIds(imdbIds);
		
		String view = controller.processForm(form, model, session);
		
		verify(filmComparisonService).compare(user, imdbIds);
		verify(model).addAttribute(eq("ratedFilms"), isA(List.class));
		
		assertEquals("comparison", view);
	}

}
