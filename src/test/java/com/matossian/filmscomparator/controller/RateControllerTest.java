package com.matossian.filmscomparator.controller;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.matossian.filmscomparator.form.RateForm;
import com.matossian.filmscomparator.model.Film;
import com.matossian.filmscomparator.model.User;
import com.matossian.filmscomparator.service.FilmComparisonService;
import com.matossian.filmscomparator.service.FilmServiceException;

public class RateControllerTest {

	@Test
	public void testRateFilm() throws FilmServiceException {
		RateController rateController = new RateController();
		FilmComparisonService filmComparisonService = mock(FilmComparisonService.class);
		rateController.setFilmComparisonService(filmComparisonService);
		Model model = mock(Model.class);
		
		String view = rateController.rateFilm(model, "tt0499549");
		
		verify(model).addAttribute(eq("rateForm"), isA(RateForm.class));
		verify(filmComparisonService).findByImdbId("tt0499549");
		
		assertEquals("rate", view);
	}
	
	@Test
	public void testProcessForm() throws FilmServiceException {
		RateController rateController = new RateController();
		HttpSession session = mock(HttpSession.class);
		FilmComparisonService filmComparisonService = mock(FilmComparisonService.class);
		rateController.setFilmComparisonService(filmComparisonService);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		RateForm rateForm = new RateForm();
		rateForm.setImdbId("tt0499549");
		rateForm.setRating(8.6);
		
		Film film = new Film();
		film.setTitle("Avatar");
		film.setImdbId("tt0499549");
		User user = new User();
		user.setUsername("gmatossian");
		
		when(session.getAttribute("user")).thenReturn(user);
		when(filmComparisonService.findByImdbId("tt0499549")).thenReturn(film);
		
		String view = rateController.processForm(rateForm, session, redirectAttributes);
		
		verify(filmComparisonService).rate(user, film, 8.6);
		verify(redirectAttributes).addFlashAttribute("successMessage", "Rating saved correctly");
		
		assertEquals("redirect:/search", view);
	}

}
